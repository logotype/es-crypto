/*!
 * sha512-es
 * https://github.com/logotype/es-crypto.git
 *
 * Copyright 2017 Victor Norgren
 * Released under the MIT license
 */
export class Int64 {
    constructor(h, l) {
        this.h = h;
        this.l = l;
    }

    static copy(dst, src) {
        dst.h = src.h;
        dst.l = src.l;
    }

    static shr(dst, x, shift) {
        dst.l = x.l >>> shift | x.h << 32 - shift;
        dst.h = x.h >>> shift;
    }

    static rotr(dst, x, shift) {
        dst.l = x.l >>> shift | x.h << 32 - shift;
        dst.h = x.h >>> shift | x.l << 32 - shift;
    }

    static rotl(dst, x, shift) {
        dst.l = x.h >>> shift | x.l << 32 - shift;
        dst.h = x.l >>> shift | x.h << 32 - shift;
    }

    static add(dst, x, y) {
        const w0 = (x.l & 0xffff) + (y.l & 0xffff),
            w1 = (x.l >>> 16) + (y.l >>> 16) + (w0 >>> 16),
            w2 = (x.h & 0xffff) + (y.h & 0xffff) + (w1 >>> 16),
            w3 = (x.h >>> 16) + (y.h >>> 16) + (w2 >>> 16);
        dst.l = w0 & 0xffff | w1 << 16;
        dst.h = w2 & 0xffff | w3 << 16;
    }

    static add4(dst, a, b, c, d) {
        const w0 = (a.l & 0xffff) + (b.l & 0xffff) + (c.l & 0xffff) + (d.l & 0xffff),
            w1 = (a.l >>> 16) + (b.l >>> 16) + (c.l >>> 16) + (d.l >>> 16) + (w0 >>> 16),
            w2 = (a.h & 0xffff) + (b.h & 0xffff) + (c.h & 0xffff) + (d.h & 0xffff) + (w1 >>> 16),
            w3 = (a.h >>> 16) + (b.h >>> 16) + (c.h >>> 16) + (d.h >>> 16) + (w2 >>> 16);
        dst.l = w0 & 0xffff | w1 << 16;
        dst.h = w2 & 0xffff | w3 << 16;
    }

    static add5(dst, a, b, c, d, e) {
        const w0 = (a.l & 0xffff) + (b.l & 0xffff) + (c.l & 0xffff) + (d.l & 0xffff) + (e.l & 0xffff),
            w1 = (a.l >>> 16) + (b.l >>> 16) + (c.l >>> 16) + (d.l >>> 16) + (e.l >>> 16) + (w0 >>> 16),
            w2 = (a.h & 0xffff) + (b.h & 0xffff) + (c.h & 0xffff) + (d.h & 0xffff) + (e.h & 0xffff) + (w1 >>> 16),
            w3 = (a.h >>> 16) + (b.h >>> 16) + (c.h >>> 16) + (d.h >>> 16) + (e.h >>> 16) + (w2 >>> 16);
        dst.l = w0 & 0xffff | w1 << 16;
        dst.h = w2 & 0xffff | w3 << 16;
    }
}
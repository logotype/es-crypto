### Core Packages

| Package | Version | Dependencies |
|--------|-------|------------|
| [`md5-es`](/packages/md5-es) | [![npm](https://img.shields.io/npm/v/md5-es.svg?maxAge=2592000)](https://www.npmjs.com/package/md5-es) | [![Dependency Status](https://api.travis-ci.org/logotype/es-crypto.svg?path=packages/md5-es)](https://travis-ci.org/logotype/es-crypto?path=packages/md5-es) |
| [`sha1-es`](/packages/sha1-es) | [![npm](https://img.shields.io/npm/v/sha1-es.svg?maxAge=2592000)](https://www.npmjs.com/package/sha1-es) | [![Dependency Status](https://api.travis-ci.org/logotype/es-crypto.svg?path=packages/sha1-es)](https://travis-ci.org/logotype/es-crypto?path=packages/sha1-es) |
| [`sha256-es`](/packages/sha256-es) | [![npm](https://img.shields.io/npm/v/sha256-es.svg?maxAge=2592000)](https://www.npmjs.com/package/sha256-es) | [![Dependency Status](https://api.travis-ci.org/logotype/es-crypto.svg?path=packages/sha256-es)](https://travis-ci.org/logotype/es-crypto?path=packages/sha256-es) |
| [`sha512-es`](/packages/sha512-es) | [![npm](https://img.shields.io/npm/v/sha512-es.svg?maxAge=2592000)](https://www.npmjs.com/package/sha512-es) | [![Dependency Status](https://api.travis-ci.org/logotype/es-crypto.svg?path=packages/sha512-es)](https://travis-ci.org/logotype/es-crypto?path=packages/sha512-es) |

--------------------------
Built with IntelliJ IDEA Open Source License

<img src="https://s3-ap-southeast-1.amazonaws.com/www.logotype.se/assets/logo-text.svg" width="200">

The people at JetBrains supports the Open Source community by offering free licenses. Check out <a href="https://www.jetbrains.com/buy/opensource/">JetBrains Open Source</a> to apply for your project. Thank you!
